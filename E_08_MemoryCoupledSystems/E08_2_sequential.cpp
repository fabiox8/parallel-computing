#include <iostream>
#include <string>
#include <cstdlib> // std::atoi

int main( int argc, char** argv )
{
	int i, N;
	double h, x, sum, PI;
	
	// http://www.cplusplus.com/articles/DEN36Up4/
	if ( argc != 3 ) // We expect 3 arguments: the program name, "-N"  and the number of stripes
	{
		std::cerr << "Usage: " << argv[0] << "-N NUMBER OF STRIPES" << std::endl;
		return 1;
	}
	
	for( int i = 1; i < argc; i++ )
	{
		if( std::string( argv[i] ) == "-N" )
		{
			if( i + 1 < argc ) // Make sure, we aren't at the end of argv
			{
				N = std::atoi( argv[i+1] ); // Increment 'i' so we don't get the argument as the next argv[i]				
			}
			else // Uh-oh, there was no argument to the destination option.
			{
				std::cerr << "-N option requires one argument." << std::endl;
				return 1;
			}
		}		
	}
	
	h = 1.0/N;	
	sum = 0;
	
	for( int i = 1; i <= N; i++ )
	{
		x = h * ( i - 0.5 );
		sum = sum + 4 / ( 1 + x * x );
	}
	PI = h * sum;
	
	std::cout << "PI = " << PI << std::endl;
	
	return 0;
}
