#include <iostream>

int main()
{
	int i, j, k, min, max;	
	int n = 10000;

	int*** A = new int**[3];
	for( int i = 0; i < 3; i++ )
	{
		A[i] = new int*[n];
		for( int j = 0; j < n; j++ )
		{
			A[i][j] = new int[n];
			for( int k = 0; k < n; k++ )
			{
				A[i][j][k] = i*j-k;
			}
		}
	}
	
	min = A[0][0][0];
	max = A[1][1][1];
	
	for( int i = 0; i < 3; i++)
	{
		for( int j = 0; j < n; j++ )
		{
			for( int k = 0; k < n; k++ )
			{
				if ( A[i][j][k] < min ) { min = A[i][j][k]; }
				if ( A[i][j][k] > max ) { max = A[i][j][k]; }
			}
		}
	}
	
	std::cout << "min = " << min << std::endl;
	std::cout << "max = " << max << std::endl;
	
	return 0;
}