#include <omp.h>
#include <iostream>

int main()
{
	#pragma omp parallel
	{
		int numberOfThreads = omp_get_num_threads();
		int threadID = omp_get_thread_num();
		
		//#pragma omp critical
		std::cout << "Hello world from thread " << threadID << " out of " << numberOfThreads << " threads" << std::endl;
	}

	return 0;
}
