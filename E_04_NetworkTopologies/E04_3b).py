import matplotlib.pyplot as plt
import numpy as np
import math

x = np.linspace(2, 6, 5)
def sp(N):
    return 2**( N * math.log( N, 2 ) - N / 2 )

def per(N):
    return math.factorial(N)

y1 = []
y2 = []
for N in x:
    y1.append(sp(N))
    y2.append(per(N))

print y1
print y2

plt.plot(x, y1, color='b', label='#SwitchPossibilities')
plt.plot(x, y2, color='r', label='#ConnectionPossibilities')
plt.legend()
plt.show()
