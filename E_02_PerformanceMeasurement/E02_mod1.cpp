#include <omp.h>
#include <iostream>

int main()
{
	int N = 99999999;
	double* a = new double[N];
	for( int i = 0; i < N; i++ ) a[i] = i/10.0; 
	// for( int i = 0; i < N; i++ ) std::cout << a[i] << "; ";
	// std::cout << std::endl;

	double s = 0;
	#pragma omp parallel
	{
		#pragma omp for reduction( +:s )
		for( int i = 0; i < N; i++ ) s += a[i];
	}

	std::cout << "s = " << s << std::endl;
		
	return 0;
}
