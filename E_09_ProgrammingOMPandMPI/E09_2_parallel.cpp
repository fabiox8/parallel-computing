#include<iostream>
#include<mpi.h>
#include<cmath>

/*
	0---1					0---1---2			0---1---2---3
	|	|					|   |   |			|	|	|	|
	2---3					3---4---5			4---5---6---7
							|   |   |			|	|	|	|
							6---7---8			8---9---10--11
												|	|	|	|
												12--13--14--15							
*/


int leftNeighbour( int rank, int numberOfProcesses )
{				
	return int(std::sqrt(numberOfProcesses)) * ( rank / int(std::sqrt(numberOfProcesses)) ) + ( rank - 1 + int(std::sqrt(numberOfProcesses)) ) % int(std::sqrt(numberOfProcesses));
}

int rightNeighbour( int rank, int numberOfProcesses )
{		
	return int(std::sqrt(numberOfProcesses)) * ( rank / int(std::sqrt(numberOfProcesses)) ) + ( rank + 1 + int(std::sqrt(numberOfProcesses)) ) % int(std::sqrt(numberOfProcesses));
}

int lowerNeighbour( int rank, int numberOfProcesses )
{
	return ( rank + int(std::sqrt(numberOfProcesses)) ) % numberOfProcesses;
}

int higherNeighbour( int rank, int numberOfProcesses )
{
	return ( rank + ( int(std::sqrt(numberOfProcesses)) - 1 ) * int(std::sqrt(numberOfProcesses)) ) % numberOfProcesses;
}



int main()
{
	/*
	// NxN Torus -> numberOfProcesses = N*N = N²
	// 3x3 Torus -> numberOfProcesses = 3*3 = 9

	int N = 3;
	int numberOfProcesses = N*N;
	int rank = 0;
	std::cout << "left " << leftNeighbour( rank, numberOfProcesses ) << std::endl;
	std::cout << "right " << rightNeighbour( rank, numberOfProcesses ) << std::endl;
	std::cout << "higher " << higherNeighbour( rank, numberOfProcesses ) << std::endl;	
	std::cout << "lower " << lowerNeighbour( rank, numberOfProcesses ) << std::endl;
	*/
	
	MPI_Init( NULL, NULL );
	
	int numberOfProcesses; // "worldSize"
	MPI_Comm_size( MPI_COMM_WORLD, &numberOfProcesses );
	
	int N = int( std::sqrt( numberOfProcesses ) );
		
	int rank;
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	
	int val = rank + 1;
	
	int left = leftNeighbour( rank, numberOfProcesses );
	int right = rightNeighbour( rank, numberOfProcesses );
	int lower = lowerNeighbour( rank, numberOfProcesses );
	int higher = higherNeighbour( rank, numberOfProcesses );
	
	/*
	std::cout << "Rank " << rank << ": left = " << left << std::endl;
	std::cout << "Rank " << rank << ": right = " << right << std::endl;
	std::cout << "Rank " << rank << ": lower = " << lower << std::endl;
	std::cout << "Rank " << rank << ": higher = " << higher << std::endl;
	*/
	
	int sum = val;
	int tmp = val;
		
	for( int i = 0; i < N - 1; i++ )
	{
		MPI_Send( &tmp, 1, MPI_INT, left, 0, MPI_COMM_WORLD );
		MPI_Recv( &tmp, 1, MPI_INT, right, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		sum = sum + tmp;
	}
	
	tmp = sum;
	
	for( int i = 0; i < N - 1; i++ )
	{
		MPI_Send( &tmp, 1, MPI_INT, lower, 0, MPI_COMM_WORLD );
		MPI_Recv( &tmp, 1, MPI_INT, higher, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		sum = sum + tmp;
	}
	
	std::cout << "Rank " << rank << ": My sum is " << sum << std::endl;
	
	MPI_Finalize( );	
	
	return 0;
}