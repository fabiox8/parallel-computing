#include <mpi.h>
#include <iostream>

int main()
{
	// Initialize the MPI environment
	MPI_Init(NULL, NULL); // -> defines the communicator "MPI_COMM_WORLD" 

	// Get the number of processes
	int numberOfProcesses; // "worldSize"
	MPI_Comm_size( MPI_COMM_WORLD, &numberOfProcesses );
	
	// Get the rank of the process
	int rank;
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );

	// Get the name of the processor
	char nodeName[MPI_MAX_PROCESSOR_NAME];
	int nameLength;
	MPI_Get_processor_name( nodeName, &nameLength );

	// Print Hello World
	std::cout << "Hello world from node " << nodeName << ", rank " << rank << " out of " << numberOfProcesses << " processes" << std::endl;

	// Finalize the MPI environment
	MPI_Finalize();
	
	return 0;
}
