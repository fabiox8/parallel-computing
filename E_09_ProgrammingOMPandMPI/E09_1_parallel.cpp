/*

0. Is there anything, that forbids parallelization in the loops? (dependencies)
-> no

1. Which loop to parallelize?
cut domain into largest chunks as possible -> do parallelization on outermost loop [but here i loop is too small]
if we can decide, we'll never choose the innermost loop (bc. then the chunks become too small)
-> j loop

2. Where to put critical section?
-> could be put inside i loop. But also outside i loop

3. Make an OMP program out of it
3.1 Create parallel region
3.2 Decide about private/shared/firstprivate/... variables
-> j,k have to be private (i is not clear)
-> min, max have to be firstprivate (otherwise they would be uninitialized)
--> optional: try to declare min max as lastprivate as well ("I doubt, that this will work. So we need a critical section?" -Mundani)
3.3 implement synchronization (e.g. critical section) & global variables
atomic not possible, bc we have to do more than exactly 1 operation
-> create global variables gmin, gmax
-> use critical section 

*/



#include <iostream>
#include <omp.h>

//export OMP_NUM_THREADS=2

int main()
{
	int i, j, k, min, max;	
	int n = 1000;
	int gmin, gmax;
	
	int*** A = new int**[3];
	for( int i = 0; i < 3; i++ )
	{
		A[i] = new int*[n];
		for( int j = 0; j < n; j++ )
		{
			A[i][j] = new int[n];
			for( int k = 0; k < n; k++ )
			{
				A[i][j][k] = i*j-k;
			}
		}
	}
	
	gmin = A[0][0][0];
	gmax = A[0][0][0];
	
	#pragma omp parallel default(shared) private(j,k) firstprivate(min,max)
	{		
	for( int i = 0; i < 3; i++)
	{
		#pragma omp for //schedule(dynamic) nowait
		for( int j = 0; j < n; j++ )
		{
			for( int k = 0; k < n; k++ )
			{
				if ( A[i][j][k] < min ) { min = A[i][j][k]; }
				if ( A[i][j][k] > max ) { max = A[i][j][k]; }
			}
		}
	}
	#pragma omp critical
	{
		if( min < gmin ) { gmin = min; }
		if( max > gmax ) { gmax = max; }
	}	
	}
	std::cout << "gmin = " << gmin << std::endl;
	std::cout << "gmax = " << gmax << std::endl;
	
	return 0;
}