#include <mpi.h>

void myBroadcast( int val )
{
	MPI_INIT( NULL, NULL );
	
	if( root ) // This if-statement is necessary and can't be avoided
	{
		MPI_Send( val, 1, MPI_INT, down ); //Always think about blocking / non-blocking communication
		MPI_Send( val, 1, MPI_INT, right );
		MPI_Recv( tmp, 1, MPI_INT, up ); //The order of receiving actually makes no difference; So a non-blocking communication would also be okay
		MPI_Recv( tmp, 1, MPI_INT, left );
	}
	else
	{
		MPI_IRecv( tmp, 1, MPI_INT, up, req_u ); // bc of nodes on border, we need a non-blocking communication here!  // req_u = request handle up
		MPI_IRecv( tmp, 1, MPI_INT, left, req_l );
		
		MPI_Waitany( req_u, req_l ); //now make sure, that I recieved tmp at least once in order to continue
		
		MPI_Send( val, 1, MPI_INT, down );
		MPI_Send( val, 1, MPI_INT, right );	
		
		MPI_Waitall( req_u, req_l ); //wait until all communication is done
	}		
	
	MPI_FINALIZE();
}

int main()
{
	myBroadcast( 10 );
}
