#include <omp.h>
#include <iostream>

int main()
{
	int N = 99999999;
	double* a = new double[N];
	for( int i = 0; i < N; i++ ) a[i] = i/10.0; 
	// for( int i = 0; i < N; i++ ) std::cout << a[i] << "; ";
	// std::cout << std::endl;

	double s = 0;
	#pragma omp parallel
	{
		int numberOfThreads = omp_get_num_threads();
		double s_T = 0;
		int PID = omp_get_thread_num();
		for( int i = PID; i < N; i += numberOfThreads ) s_T += a[i];
		#pragma omp critical
		s += s_T;
	}

	std::cout << "s = " << s << std::endl;
		
	return 0;
}
