#include <omp.h>
#include <iostream>

int main()
{
	int N = 99999999;
	double* a = new double[N];
	for( int i = 0; i < N; i++ ) a[i] = i/10.0; 
	// for( int i = 0; i < N; i++ ) std::cout << a[i] << "; ";
	// std::cout << std::endl;

	int numberOfThreads = 4;
	//std::cout << "numberOfThreads = " << numberOfThreads << std::endl;

	double* s_T = new double[numberOfThreads];
	for( int i = 0; i < numberOfThreads; i++ ) s_T[i] = 0;
	// for( int i = 0; i < numberOfThreads; i++ ) std::cout << s_T[i] << "; ";
	// std::cout << std::endl;

	double s = 0;
	#pragma omp parallel num_threads(numberOfThreads)
	{
		int numberOfThreads = omp_get_num_threads();
		int PID = omp_get_thread_num();
		for( int i = PID; i < N; i += numberOfThreads ) s_T[PID] += a[i];
		#pragma omp critical
		s += s_T[PID];
	}

	// for( int i = 0; i < numberOfThreads; i++ ) std::cout << s_T[i] << "; ";
	// std::cout << std::endl;
	
	std::cout << "s = " << s << std::endl;
		
	return 0;
}
