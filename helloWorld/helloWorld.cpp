#include <mpi.h>
#include <omp.h>
#include <iostream>

int main()
{
	MPI_Init( NULL, NULL );
	int numberOfProcesses;
	MPI_Comm_size( MPI_COMM_WORLD, &numberOfProcesses );
	int rank;
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	char nodeName[MPI_MAX_PROCESSOR_NAME];
	int nameLength;
	MPI_Get_processor_name( nodeName, &nameLength );

	#pragma omp parallel
	{
		int numberOfThreads = omp_get_num_threads();
		int threadID = omp_get_thread_num();
		
		#pragma omp critical
		// std::cout << "Hello world from node " << nodeName << ", rank " << rank << ", thread " << threadID << " out of " << numberOfProcesses << " processes with " << numberOfThreads << " threads." << std::endl;
		std::cout << "Hello world from node " << nodeName << ", rank " << rank << " out of " << numberOfProcesses << " processes (thread " << threadID << " of " << numberOfThreads << " threads/process)." << std::endl;
	}

	MPI_Finalize();

	return 0;
}
